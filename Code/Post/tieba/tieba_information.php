<?php
header("Content-type: text/html; charset=utf-8"); 
class TiebaInformation{
	var $tid,$uid,$title,$content,$public,$focus,$link;
	public function __construct(){ 
		$this->link = new ConnData();
	}
	
	public function searchInit($tid){  /*查询tieba*/
		$this->tid=$tid;
		$sql="select uid,title,content,public,focus from themes where tid=".$tid;	
		$result = mysqli_query($this->link->conn,$sql);
		if($result === false){
    		echo $this->link->conn->error;
    		echo $this->link->conn->errno;
    		return;
		}
		$row = mysqli_fetch_array($result,MYSQLI_ASSOC);
    	$this->uid=$row['uid'];
    	$this->title=$row['title'];
    	$this->content=$row['content'];
		$this->public=$row['public'];
		$this->focus=$row['focus'];
	}

	public function insertInit($uid,$title,$content,$public,$focus){  /*创建tieba*/
		
		$this->uid=$uid;
    	$this->title=$title;
    	$this->content=$content;
		$this->public=$public;
		$this->focus=$focus;
		
 		$this->insertToThemes();
 		$this->getId();
 		/*$this->createComment();*/
 		
	}

	private function insertToThemes(){
		$sql="insert into themes(uid,title,content,public,focus) values(".$this->uid.",'".$this->title."','".$this->content."','".$this->public."',".$this->focus.");";
		$result = mysqli_query($this->link->conn,$sql);
		if($result === false){
    		echo $this->link->conn->error;
    		echo $this->link->conn->errno;
		}
	}
	private function getId(){
		$sql="select last_insert_id()";
		$result = mysqli_query($this->link->conn,$sql);
		if($result === false){
    		echo $this->link->conn->error;
    		echo $this->link->conn->errno;
    		return;
		}
		$row = mysqli_fetch_array($result,MYSQLI_NUM);
    	$this->tid=$row[0];
	}
/*	private function createComment(){
		$str=(string)$this->tid;
    	$sql="create table comment".$str." (cid int(11) unsigned not null auto_increment,uid int(11) unsigned not null,tid int(11) unsigned not null,content varchar(1023) not null,public datetime not null,primary key(cid))";
    	$result = mysqli_query($this->link->conn,$sql);
    	if($result === false){
    		echo $this->link->conn->error;
    		echo $this->link->conn->errno;
    		return;
		}
	}*//*uid没有处理*/
	public function showInf($content_css){
		echo '<div><h1>'.$this->title.'</h1>'.
		'<h2>发布者：'.$this->uid.'</h2>'.
		'<h3>发布时间：'.$this->public.'；评论和关注的人数：'.$this->focus.'</h3>'.
		'<p '.$content_css.'>'.$this->content.'</p><div>';/*uid改成username的链接*/	
	}

	public function deleteTieba(){
		$sql="delete from themes where tid=".$this->tid;
		$result = mysqli_query($this->link->conn,$sql);
		if($result === false){
    		echo $this->link->conn->error;
    		echo $this->link->conn->errno;
		}
		$this->deleteComment($this->tid);
    	
	}
	private function deleteComment($tid){
		$sql="delete from comments where tid=".$this->tid;
		$result = mysqli_query($this->link->conn,$sql);
		if($result === false){
    		echo $this->link->conn->error;
    		echo $this->link->conn->errno;
		}
	}
}

?>