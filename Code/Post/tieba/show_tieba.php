<?php
class ShowTieba extends ShowList{
	public function __construct($num,$theme_css,$page_css,$between_page,$where,$order,$page_urls){  /*css为完整的css格式。style:'...'*/
		$counts=$this->countNum("themes",$where);
		$last_page=ceil($counts/$num);
		$now_page=$_GET['page'];
		$from=($now_page-1)*$num;
		if($now_page==$last_page)$num=$counts-$from;
		$this->showTiebas($theme_css,$from,$num,$where,$order);
		$pages=new ShowPage($between_page,$now_page,$last_page,$page_urls);
		$pages->showLink($page_css);
	}

	private function showTiebas($css,$from,$num,$where,$order){  /*css为完整的css格式。style:'...'*/
		$sql="select title,tid from themes ".$where." ".$order." limit ".$from.",".$num;
		$result=$this->sqlInit($sql);
		$this->showList($css,$result);
	}
	private function showList($css,$result){  /*css为完整的css格式。style:'...'*/
		while($row = $result->fetch_assoc()){
    		$tid=$row['tid'];
    		$title=$row['title'];
    		echo "<div><p ".$css."><a href=\"detail_information.php?tid=".$tid."&page=1\">".$title."</a></p></div>\n";
		}
	}
	
}
?>