<?php
class ShowComment extends ShowList{
	var $tables;
	public function __construct($num,$tid,$comment_css,$end_css,$page_css,$between_page,$where,$order,$page_urls){  /*css为完整的css格式。style:'...'*/
		$this->tables="comments";
		$counts=$this->countNum($this->tables,$where);
		$last_page=ceil($counts/$num);
		$now_page=$_GET['page'];
		$from=($now_page-1)*$num;
		if($now_page==$last_page)$num=$counts-$from;
		$this->showComments($comment_css,$end_css,$from,$num,$where,$order);
		$pages=new ShowPage($between_page,$now_page,$last_page,$page_urls);
		$pages->showLink($page_css);
	}

	private function showComments($content_css,$end_css,$from,$num,$where,$order){  /*css为完整的css格式。style:'...'*/
		$sql="select content,uid,public from comments ".$where." ".$order." limit ".$from.",".$num;
		$result=$this->sqlInit($sql);
		$this->showList($content_css,$end_css,$result);
	}
	private function showList($content_css,$end_css,$result){  /*css为完整的css格式。style:'...'*/
		while($row = $result->fetch_assoc()){
    		$uid=$row['uid'];
    		$content=$row['content'];
    		$public=$row['public'];
    		echo "<div><p ".$content_css.">".$content."</p>\n<p ".$end_css.">".$uid."&nbsp".$public."</p></div>\n"; /*uid改成username的链接*/
		}
	}	
}
?>