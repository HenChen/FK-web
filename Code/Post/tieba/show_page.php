<?php
header("Content-type: text/html; charset=utf-8"); 
class ShowPage{
	var $links=array();
	var $now_page,$between_page,$last_page;
	var $page_urls;
	public function __construct($between_page,$now_page,$last_page,$page_urls){
		$this->now_page=$now_page;
		$this->between_page=$between_page;
		$this->last_page=$last_page;
		$this->arrayInit($page_urls);
	}
	private function arrayInit($page_urls){
		$begin;$end;
		if($this->now_page>1){
			array_push($this->links,"<a href=\"".$page_urls."page=".(string)($this->now_page-1)."\">上一页</a>");
			array_push($this->links,"<a href=\"".$page_urls."page=1\">第一页</a>");
		}
		if($this->now_page>$this->between_page+1){
			array_push($this->links,"...");
			$begin=$this->now_page-$this->between_page;
		}
		else $begin=1;
		if($this->now_page<$this->last_page-$this->between_page){
			$end=$this->now_page+$this->between_page;
			$this->pageNum($begin,$end,$page_urls);
			array_push($this->links,"...");
		}
		else{
			$end=$this->last_page;
			if($end-2*$this->between_page>0)$begin=$end-2*$this->between_page;
			$this->pageNum($begin,$end,$page_urls);
		}
		if($this->now_page!=$this->last_page){
			array_push($this->links,"<a href=\"".$page_urls."page=".(string)$this->last_page."\">最后一页</a>");
			array_push($this->links,"<a href=\"".$page_urls."page=".(string)($this->now_page+1)."\">下一页</a>");
		}
	}
	private function pageNum($begin,$end,$page_urls){
		for($i=$begin;$i<=$end;++$i){
			$str=(string)$i;
			if($i==$this->now_page){
				array_push($this->links,$str);
				continue;
			}
			array_push($this->links,"<a href=\"".$page_urls."page=".$str."\">".$str."</a>");
		}
	}
	public function showLink($css){  /*css为完整的css格式。style:'...'*/
		$str="<div><p ".$css.">";
		foreach ($this->links as $value) $str.=$value."&nbsp\n";
		$str.="</p></div>";
		echo $str;
	}
}

?>