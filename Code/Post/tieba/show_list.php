<?php
header("Content-type: text/html; charset=utf-8"); 
class ShowList{
	protected function sqlInit($sql){	
		$link = new ConnData();
		$result = mysqli_query($link->conn,$sql);
		if($result === false){
    		echo $link->conn->error;
    		echo $link->conn->errno;
		}
		return $result;
	}
	protected function countNum($table,$where){
		$sql="select count(*) from ".$table." ".$where;
		$result=$this->sqlInit($sql);
		$row = mysqli_fetch_array($result,MYSQLI_NUM);
    	return $row[0];
	}
}
?>