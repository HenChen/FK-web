/*Table structure for table `userdata` */

DROP TABLE IF EXISTS `userdata`;

CREATE TABLE `userdata` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `password` varchar(50) NOT NULL,
  `realname` varchar(30) DEFAULT NULL,
  `role` int(11) DEFAULT '0',
  `headportrait` varchar(100) DEFAULT NULL,
  `giturl` varchar(100) DEFAULT NULL,
  `application` date DEFAULT NULL,
  `invitationnum` int(11) DEFAULT '0',
  `mission` int(11) DEFAULT NULL,
  `homework` float DEFAULT NULL,
  `commentsnum` int(11) DEFAULT '0',
  `activetime` datetime DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `leve` int(11) DEFAULT '0',
  `fkMoney` float DEFAULT '0',
  `experiencevalue` float DEFAULT '0',
  `phone` varchar(20) DEFAULT NULL,
  `qq` varchar(20) DEFAULT NULL,
  `signature` varchar(200) DEFAULT NULL,
  `sex` enum('男','女') DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `id` (`uid`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

/*Data for the table `userdata` */

insert  into `userdata`(`uid`,`username`,`password`,`realname`,`role`,`headportrait`,`giturl`,`application`,`invitationnum`,`mission`,`homework`,`commentsnum`,`activetime`,`birthday`,`email`,`leve`,`fkMoney`,`experiencevalue`,`phone`,`qq`,`signature`,`sex`) values (8,'欧阳浩','40bd001563085fc35165329ea1ff5c5ecbdbbeef',NULL,0,NULL,NULL,NULL,0,NULL,NULL,0,NULL,NULL,NULL,0,0,0,NULL,NULL,NULL,NULL),(6,'刘鑫','40bd001563085fc35165329ea1ff5c5ecbdbbeef',NULL,0,NULL,NULL,NULL,0,NULL,NULL,0,NULL,NULL,NULL,0,0,0,NULL,NULL,NULL,NULL),(5,'ljc','40bd001563085fc35165329ea1ff5c5ecbdbbeef',NULL,0,NULL,'www.baidu.com',NULL,0,NULL,NULL,0,NULL,NULL,'654f56456@qq.com',0,0,0,'','45646568','dawdwqad','男'),(7,'吴清','40bd001563085fc35165329ea1ff5c5ecbdbbeef',NULL,0,NULL,NULL,NULL,0,NULL,NULL,0,NULL,NULL,NULL,0,0,0,NULL,NULL,NULL,NULL),(9,'who','40bd001563085fc35165329ea1ff5c5ecbdbbeef',NULL,0,NULL,NULL,NULL,0,NULL,NULL,0,NULL,NULL,NULL,0,0,0,NULL,NULL,NULL,NULL),(10,'周宇','40bd001563085fc35165329ea1ff5c5ecbdbbeef',NULL,0,NULL,NULL,NULL,0,NULL,NULL,0,NULL,NULL,NULL,0,0,0,NULL,NULL,NULL,NULL),(11,'周久迦','40bd001563085fc35165329ea1ff5c5ecbdbbeef',NULL,0,NULL,NULL,NULL,0,NULL,NULL,0,NULL,NULL,NULL,0,0,0,NULL,NULL,NULL,NULL),(12,'郑灿','40bd001563085fc35165329ea1ff5c5ecbdbbeef',NULL,0,NULL,NULL,NULL,0,NULL,NULL,0,NULL,NULL,NULL,0,0,0,NULL,NULL,NULL,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
