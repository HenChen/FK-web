function ShowLevel(li)
{
	var menu = li.getElementsByTagName('ul')[0];
	menu.style.display = 'block';
}
function HideSub(li)
{
	var menu = li.getElementsByTagName('ul')[0];
	menu.style.display = 'none';
}
window.onload = function() {
    var list = document.getElementById('banner__carousel_list');
    var prev = document.getElementById('prev');
    var next = document.getElementById('next');
    var buttons = document.getElementById('banner__carousel_buttons').getElementsByTagName('span');
    var index = 1;
    var timer;
    var s=1093;
    var animated = false;
    //自动播放
    function play() {
        timer = setInterval(function () {next.onclick();},3000);
            }
    function stop(){
    	clearInterval(timer);
    }
    play();
    banner__carousel.onmouseout = play;
	banner__carousel.onmouseover = stop;
	//左右切换，无限滚动
    function animate(offset) {
    	animated=true;
    	var newLeft = parseInt(list.style.left) + offset;
    	var time = 300;
    	var interval = 3;
    	var speed = offset/(time/interval);
    	var go=function(){
    		if( (speed > 0 && parseInt(list.style.left) < newLeft)||(speed < 0 && parseInt(list.style.left) > newLeft) ){
	    		list.style.left = parseInt(list.style.left)+speed+'px';
	    		setTimeout(go,interval);
	    	}
	    	else{
	    		animated=false;
	    		list.style.left = newLeft + 'px';
		        if(newLeft>-s){
		        	list.style.left = -5*s +'px';
		        }
		        if(newLeft<-5*s){
		        	list.style.left = -s +'px';
		        }
	    	}
	    }
    	go();
        
    }
   	prev.onclick = function() {  
   		index -= 1;
   		if(index<1){index=5;}
   		showButton();  
   		if(!animated)   
        animate(s);
    }
    next.onclick = function() {
    	index += 1;
    	if(index>5){index=1;}
    	showButton();  
    	if(!animated)  
        animate(-s);
    }

    function showButton(){
		for (var i = 0; i < buttons.length; i++) {
            if (buttons[i].className == 'on') {
                buttons[i].className = '';
            }
        }
        buttons[index - 1].className = 'on';
    }
    //按钮切换
    for (var i = 0; i < buttons.length; i++) {
        buttons[i].onclick = function () {
        if(this.className == 'on') {
            return;
        }
	        var myIndex = parseInt(this.getAttribute('index'));
            
	        var offset = -s * (myIndex - index);
			animate(offset);
	        index = myIndex;

	        showButton();
        }
    }
    console.log(this);
    
}