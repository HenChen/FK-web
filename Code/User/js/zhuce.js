
    function checkuse() {
        var check;
        var partten =   /^[a-zA-Z0-9_-]{4,16}$/;
        var username = document.getElementById("username").value;
        if (username.length>16 || username.length<6){
            document.getElementById("usernameInfo").innerHTML = "必须为6-16位之间！";
            check = false;
        }
        else if(!partten.test(username)){
            document.getElementById("usernameInfo").innerHTML = "必须由数字和字母组成！";
            check = false;
        }else {
        	document.getElementById("usernameInfo").innerHTML = "";
            check = true;
        }
        return check;
    }
    function checkpwd() {
        var check;
        var pass = document.getElementById("password").value;
        var pattern = /^[\@A-Za-z0-9\!\#\$\%\^\&\*\.\~]{6,22}$/;
        if(pass.length<6 ||pass.length>22 ){
            document.getElementById("passwordInfo").innerHTML = "密码为6-22位间";
            check = false;
        }
        if(!pattern.test(pass)){
            document.getElementById("passwordInfo").innerHTML = "密码不符合要求";
            check = false;
        }else {
        	document.getElementById("passwordInfo").innerHTML = "";
            check = true;

        }
        return check;
    }
    function checkpwdc() {
        var check;
        var pass = document.getElementById("password").value;
        var pwdc = document.getElementById("pwdc").value;

        if(pwdc != pass){
            document.getElementById("pwdcInfo").innerHTML = "两次密码输入不一致";
            check = false;
        }else {
            check = true;
        }
        return check;
    }
    function checkemail() {
        var check;
        var email = document.getElementById("email").value;
        var pattern = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if(!pattern.test(email)){
            document.getElementById("emailInfo").innerHTML = "输入邮箱格式错误";
            check = false;

            }

        else {
            check = true;
            document.getElementById("emailInfo").innerHTML = "";
        }
        return check;
    }
    function checkphone(){
    	var check;
    	var phone = document.getElementById("phone").value;
    	var pattern = /^((13[0-9])|(14[5|7])|(15([0-3]|[5-9]))|(18[0,5-9]))\d{8}$/;
    	if(!pattern.test(phone)){
    		document.getElementById("phoneInfo").innerHTML = "请输入正确的手机号";
    		check = false;
    	}
    	else{
    		check = true;
    		document.getElementById("phoneInfo").innerHTML = "";
    	}
    	return check;
    }
    function checkqq(){
    	var check;
    	var qq = document.getElementById("qq").value;
    	var pattern = /^[1-9][0-9]{4,10}$/;
    	if(!pattern.test(qq)){
    		document.getElementById("qqInfo").innerHTML = "请输入正确的QQ号";
    		check = false;
    	}
    	else{
    		check = true;
    		document.getElementById("qqInfo").innerHTML = "";
    	}
    	return check;
    }
  function check() {
      var check = checkuse() && checkpwd() && checkpwdc() && checkemail() && checkphone() && checkqq();
      return check;
  }
    function sub(){
    	var check = checkuse() && checkpwd() && checkpwdc() && checkemail() && checkphone() && checkqq();
    	if(!check){
//////  		document.regist.submit()="登陆.html";
////  		location.href=("登陆.html");
//			window.location.href("登陆.html");
			alert("请填写完整注册信息");
    	}
    	else{
    		return true;
    	}
    }
