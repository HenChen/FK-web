
	var name = jQuery("#username").val();
	/*
		^  正则表达式的开始符号
		$  表示结束符号
		\w 表示一个数字字母下划线
	*/
	//1-14位的数字字母或者下划线
	var reg = /^([\u4e00-\u9fa5\w]{1,7}|\w{1,14})$/;/*定义一个正则表达式*/
	// reg.test(name) 用正则表达式去判断一个字符串是否符合规则返回true和false
	if(  $("#username").val() == null ||  $("#username").val() == "" ){//如果用户名为空
		$("#ulbl").text("用户名不为空");
	}else{//如果用户名不为空  ，才进行正则表达式的判断
		if( reg.test(name) ){//如果正则表达式能通过匹配，就将错误显示出来
			$("#ulbl").text("*");
		}else{
			
		 	$("#ulbl").text("1-14位的数字字母或者下划线");
		}
	}
	//alert(name);
});

//密码框的失去焦点事件
$("#password").blur(function(){
		var pwd = $("#password").val();//获取密码框输入的值
		var reg = /^[\w!@#$^&]{6,18}$/;//密码6到18位
		if(pwd == null || pwd == ""){
			$("#plbl").text("密码不能为空");
		}else{//不为空的情况
			if( reg.test(pwd) ){//判断正则表达式是否能通过
				$("#plbl").text("*");
			}else{
				$("#plbl").text("密码必须为6-18位，不能是中文");
			}
			
		}
	
});

//密码框的失去焦点事件
$("#password2").blur(function(){
		var pwd = $("#password2").val();//获取确认密码框输入的值
		var pwd2 = $("#password").val();
		if(pwd == null || pwd == ""){
			$("#plbl2").text("密码不能为空");
		}else{//不为空的情况
			if( pwd == pwd2 ){//判断正则表达式是否能通过
				$("#plbl2").text("*");
			}else{
				$("#plbl2").text("两次输入的密码不一致");
			}
			
		}
	
});

//电子邮箱的失去焦点事件
$("#email").blur(function(){
		var email = $("#email").val();//获取密码框输入的值
		var reg = /^.{1,30}@.{2,10}(\..{2,10})+$/;//密码6到18位   .任意字符   1-30位    @   .{2,20}  +一到多个
		if(email == null || email == ""){
			$("#elbl").text("邮箱不能为空");
		}else{//不为空的情况
			if( reg.test(email) ){//判断正则表达式是否能通过
				$("#elbl").text("*");
			}else{
				$("#elbl").text("请输入正确的邮箱格式");
			}
			
		}
	
});

/*验证码格式验证*/
$("#code").blur(function(){
		var code = $("#code").val();//获取密码框输入的值
		var reg = /^[0-9a-zA-Z]{4}$/;// \d数字
		if(code == null || code == ""){
			$("#clbl").text("验证码不能为空");
		}else{//不为空的情况
			if( reg.test(code) ){//判断正则表达式是否能通过
				$("#clbl").text("*");
			}else{
				$("#clbl").text("请输入4位验证码");
			}
			
		}
	
});

/*在点击图片验证码的时候  刷新*/
$("#imageCode").click(function(){
	var img = "imageCode.do?t="+Math.random();
	$("#imageCode").attr("src",img);
	
});